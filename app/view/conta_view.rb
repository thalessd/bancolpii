require_relative 'tela'
require_relative 'principal'
require_relative '../model/conta'
require_relative '../model/agencia'
require_relative '../model/util'

class ContaView < Tela
  def menu
    begin
      @prompt.select('Conta', [
          { name: 'Criar', value: 1 },
          { name: 'Listar', value: 2 },
          { name: 'Atualizar', value: 3 },
          { name: 'Deletar', value: 4 },
          { name: 'Depositar', value: 5 },
          { name: 'Sacar', value: 6 },
          { name: 'Transferir', value: 7 },
          { name: '< Voltar', value: 8 }
      ],per_page: 8, help: "(Use as setas do teclado)")
    rescue
      puts "\n[OPERAÇÃO CANCELADA POR O USUÁRIO]"
      @reader.read_line('< Voltar')
      8
    end
  end

  def criar_conta
    puts 'Criar uma nova conta'

    a = Agencia.listar
    a = a.reverse

    puts 'Nome da Agência:0000-X'

    idx_ag_sel = prompt.select("Qual agência deseja criar a conta?", per_page: 6) do |m|
      a.each_with_index do |ag, i|
        m.choice "#{ag.nome}:#{ag.numero}", i
      end
    end

    ag_sel = a[idx_ag_sel]

    r = @prompt.collect do
      key(:numero).ask('Numero (9999-X): ') do |q|
        q.validate /^[0-9]{4}-[0-9X]$/, 'Número de Agência inválido'
      end
      key(:limite).ask('Limite: ', default: 5000)
      key(:rendimento).ask('Rendimento: ', default: 0)
    end

    c = Conta.new(r[:numero])
    c.limite = r[:limite]
    c.rendimento = r[:rendimento]

    puts c.salvar(ag_sel) ? 'Conta salva com sucesso!' : 'Não foi possivel salvar a conta'
    @reader.read_line('< Voltar')

    ContaView.new

  end

  def listar_conta
    puts 'Lista de Contas'
    table = []
    c = Conta.listar

    table << ['Número', 'Saldo', 'Limite', 'Rendimento', 'Nome da Agência']
    table << :separator

    c.each do |ct|
      table << [ct.numero, ct.saldo, ct.limite, ct.rendimento, ct.nome_agencia]
    end

    table << :separator
    table << ["Contas cadastradas: #{c.length}", '']

    puts @table.new :rows => table

    @reader.read_line('< Voltar')

    ContaView.new
  end

  def atualizar_conta
    puts 'Atualizar Conta'

    c = Conta.listar
    c = c.reverse

    puts '0000-X'

    idx_ct_sel = prompt.select("Qual conta deseja atualizar?", per_page: 6) do |m|
      c.each_with_index do |ct, i|
        m.choice "#{ct.numero}", i
      end
    end

    ct_sel = c[idx_ct_sel]

    r = @prompt.collect do

      key(:numero).ask('Numero (9999-X): ', default: ct_sel.numero) do |q|
        q.validate /^[0-9]{4}-[0-9X]$/, 'Número de Agência inválido'
      end

      key(:limite).ask('Limite: ', default: ct_sel.limite)
      key(:rendimento).ask('Rendimento: ', default: ct_sel.rendimento)
    end

    transferir_conta = @prompt.yes?('Deseja traansferir a conta de agência?')

    ct_nova = Conta.new(r[:numero])
    ct_nova.id = ct_sel.id
    ct_nova.limite = r[:limite]
    ct_nova.rendimento = r[:rendimento]

    if transferir_conta
      puts 'Nome da Agência:0000-X'
      a = Agencia.listar

      idx_ag_sel = prompt.select("Qual agência deseja criar a conta?", per_page: 6) do |m|
        a.each_with_index do |ag, i|
          m.choice "#{ag.nome}:#{ag.numero}", i
        end
      end

      ag_sel = a[idx_ag_sel]

      ct_nova.id_agencia = ag_sel.id
    else
      ct_nova.id_agencia = ct_sel.id_agencia
    end

    puts ct_nova.atualizar ? 'Conta atualizada com Sucesso' : 'Não foi possivel atualizar a conta'
    @reader.read_line('< Voltar')

    ContaView.new
  end

  def deletar_conta
    puts 'Deletar Conta'

    c = Conta.listar
    c = c.reverse

    puts '0000-X'

    idx_ct_sel = prompt.select("Qual conta deseja deletar?", per_page: 6) do |m|
      c.each_with_index do |ct, i|
        m.choice "#{ct.numero}", i
      end
    end

    ct_sel = c[idx_ct_sel]

    if @prompt.yes?("Deseja deletar a conta #{ct_sel.numero}?")
      puts ct_sel.deletar ? 'Conta deletada com sucesso' : 'Não foi possível deletar a conta'
    end
    @reader.read_line('< Voltar')

    ContaView.new
  end

  def depositar_em_conta
    puts 'Depósito em Conta'

    c = Conta.listar
    c = c.reverse

    puts '0000-X'

    idx_ct_sel = prompt.select("Qual conta deseja sacar?", per_page: 6) do |m|
      c.each_with_index do |ct, i|
        m.choice "#{ct.numero}", i
      end
    end

    ct_sel = c[idx_ct_sel]

    limite_restante = ct_sel.limite - ct_sel.saldo

    puts "Saldo: #{ct_sel.saldo}"
    puts "Limite Restante: #{limite_restante}"

    val_deposito = @prompt.ask('Valor do depósito') do |a|
      a.in "1-#{limite_restante}"
      a.messages[:range?] = 'O valor ecede o limite!'
    end

    puts ct_sel.depositar(val_deposito) ? 'Valor depositado' : 'Não foi possível depositar'
    @reader.read_line('< Voltar')
    ContaView.new
  end

  def saque_em_conta
    puts 'Saque em Conta'

    c = Conta.listar
    c = c.reverse

    puts '0000-X'

    idx_ct_sel = prompt.select("Qual conta deseja sacar?", per_page: 6) do |m|
      c.each_with_index do |ct, i|
        m.choice "#{ct.numero}", i
      end
    end

    ct_sel = c[idx_ct_sel]

    puts "Saldo: #{ct_sel.saldo}"

    val_saque = @prompt.ask('Valor do saque') do |a|
      a.in "1-#{ct_sel.saldo}"
      a.messages[:range?] = 'A conta não possui dinheiro o suficiente!'
    end

    puts ct_sel.sacar(val_saque) ? 'Valor sacado' : 'Não foi possível sacar'
    @reader.read_line('< Voltar')
    ContaView.new

  end

  def transferencia_em_conta
    puts 'Transferência entre Contas'

    c = Conta.listar
    c = c.reverse

    puts '0000-X'

    idx_ct_sel = prompt.select("Transferir de: ", per_page: 6) do |m|
      c.each_with_index do |ct, i|
        m.choice "#{ct.numero}", i
      end
    end

    ct_from = c[idx_ct_sel]
    c -= [ct_from]

    idx_ct_sel = prompt.select("Transferir para: ", per_page: 6) do |m|
      c.each_with_index do |ct, i|
        m.choice "#{ct.numero}", i
      end
    end

    ct_to = c[idx_ct_sel]

    puts "Saldo da Conta #{ct_from.numero}: #{ct_from.saldo}"

    val_transf = @prompt.ask('Valor da transferência:') do |a|
      a.in "1-#{ct_from.saldo}"
      a.messages[:range?] = 'A conta não possui dinheiro o suficiente!'
    end

    puts ct_from.transferir(ct_to, val_transf) ? 'Transferência Realizada com Sucesso' : 'Não foi possível transferir'
    @reader.read_line('< Voltar')
    ContaView.new
  end

  def initialize
    super
    u = Util.new
    u.limpar_tela
    num = menu
    u.limpar_tela

    begin
      case num
      when 1
        criar_conta
      when 2
        listar_conta
      when 3
        atualizar_conta
      when 4
        deletar_conta
      when 5
        depositar_em_conta
      when 6
        saque_em_conta
      when 7
        transferencia_em_conta
      else
        Principal.new
      end
    rescue
      puts "\n[NÃO FOI POSSÍVEL COMPLETAR A AÇÃO]"
      @reader.read_line('< Voltar')
      ContaView.new
    end
  end
end