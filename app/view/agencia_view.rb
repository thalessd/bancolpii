
require_relative 'tela'
require_relative '../model/agencia'
require_relative '../model/util'

class AgenciaView < Tela
    def menu
        begin
            @prompt.select('Agência', [
                { name: 'Criar', value: 1 },
                { name: 'Listar', value: 2 },
                { name: 'Atualizar', value: 3 },
                { name: 'Deletar', value: 4 },
                { name: '< Voltar', value: 5 }
            ], help: "(Use as setas do teclado)")
        rescue
            puts "\n[OPERAÇÃO CANCELADA POR O USUÁRIO]"
            5
        end
    end
    def criar_agencia
        puts 'Criar nova agência'

        r = @prompt.collect do
            key(:numero).ask('Numero (6 Digítos): ') do |q|
                q.validate /^[0-9]{6}$/, 'Número de Agência inválido'
            end
            key(:agencia_nome).ask('Nome: ', required: true)
        end

        a = Agencia.new(r[:numero])
        a.nome = r[:agencia_nome]

        puts a.salvar ? 'Agencia criada com Sucesso' : 'Não foi possível criar a Agência'
        @reader.read_line('< Voltar')
        AgenciaView.new
    end
    def listar_agencia
        puts 'Lista de agências'
        table = []
        a = Agencia.listar

        table << %w(Número Nome)
        table << :separator

        a.each do |ag|
            table << [ag.numero, ag.nome]
        end

        table << :separator
        table << ["Total: #{a.length}", '']

        puts @table.new :rows => table

        @reader.read_line('< Voltar')

        AgenciaView.new
    end
    def atualizar_agencia
        puts 'Atualizar Agência'
        a = Agencia.listar
        a = a.reverse

        puts 'Nome da Agência:0000-X'

        idx_ag_sel = prompt.select("Qual agência deseja atualizar?", per_page: 6) do |m|
            a.each_with_index do |ag, i|
                m.choice "#{ag.nome}:#{ag.numero}", i
            end
        end

        ag_sel = a[idx_ag_sel]

        r = @prompt.collect do
            key(:numero).ask('Numero (6 Digítos): ',
                             default: ag_sel.numero,
                             validate: /^[0-9]{6}$/
            )
            key(:agencia_nome).ask('Nome: ',
                                   default: ag_sel.nome
            )
        end

        ag_nova = Agencia.new(r[:numero])
        ag_nova.nome = r[:agencia_nome]
        ag_nova.id = ag_sel.id

        puts ag_nova.atualizar ? 'Agência atualizada com Sucesso' : 'Não foi possível atualizar a agência!'

        @reader.read_line('< Voltar')
        AgenciaView.new
    end
    def deletar_agencia
        puts 'Deletar agência'

        a = Agencia.listar
        a = a.reverse

        puts 'Nome da Agência:0000-X'

        idx_ag_sel = prompt.select("Qual agência deseja deletada?", per_page: 6) do |m|
            a.each_with_index do |ag, i|
                m.choice "#{ag.nome}:#{ag.numero}", i
            end
        end

        ag_sel = a[idx_ag_sel]

        ct = ag_sel.listar_contas

        if ct.length > 0
            table = []
            table << ['Número', 'Saldo', 'Limite', 'Rendimento', 'Nome Agência']
            table << :separator
            ct.each do |c|
                table << [c.numero, c.saldo, c.limite, c.rendimento, c.nome_agencia]
            end

            puts @table.new :rows => table, :title => 'Contas Cadastradas'
        end

        if @prompt.yes?("Deseja mesmo deletar a agência #{ag_sel.nome}")
            puts ag_sel.deletar ? 'Deletado com Sucesso' : 'Falha ao Deletar'
        end
        @reader.read_line('< Voltar')
        AgenciaView.new
    end

    def initialize
        super
        u = Util.new
        u.limpar_tela

        acao = menu

        u.limpar_tela

        begin
            case acao
            when 1
                criar_agencia
            when 2
                listar_agencia
            when 3
                atualizar_agencia
            when 4
                deletar_agencia
            else
                Principal.new
            end
        rescue
            puts "\n[NÃO FOI POSSÍVEL COMPLETAR A AÇÃO]"
            @reader.read_line('< Voltar')
            AgenciaView.new
        end
    end
end