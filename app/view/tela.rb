require 'tty-reader'
require 'tty-prompt'
require 'terminal-table'

class Tela

  attr_accessor :prompt, :reader, :table

  def initialize
    @reader = TTY::Reader.new
    @prompt = TTY::Prompt.new
    @table =  Terminal::Table
  end

  def menu
    begin
      @prompt.select('Conta', [
          { name: '< Voltar', value: -1 }
      ], help: "(Use as setas do teclado)")
    rescue
      puts "\n[OPERAÇÃO CANCELADA POR O USUÁRIO]"
      -1
    end
  end

end