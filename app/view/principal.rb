require_relative 'tela'
require_relative 'agencia_view'
require_relative 'conta_view'
require_relative '../model/util'


class Principal < Tela

  def initialize
    super
    u = Util.new
    u.limpar_tela
    puts "Trabalho LP II"
    puts "-" * 10

    num = @prompt.select('Menu', [
        {name: 'Agencia', value: 1},
        {name: 'Conta', value: 2},
        {name: 'Sair', value: 3}
    ], help: "(Use as setas do teclado)")

    case num
    when 1
      AgenciaView.new
    when 2
      ContaView.new
    else
      exit(0)
    end
  end
end