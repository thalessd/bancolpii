require_relative 'banco'
require_relative 'conta'

class Conta
  attr_reader :id, :numero, :saldo, :limite, :rendimento, :nome_agencia, :numero_agencia, :id_agencia
  attr_writer :id, :saldo, :limite, :rendimento, :nome_agencia, :numero_agencia, :id_agencia

  def initialize(numero)
    @numero = numero
    @db = Banco.new
    @saldo = 0
  end

  def self.listar(agencia = nil)
    contas = []
    db = Banco.new

    begin
      query = "select conta.id as id_conta, conta.numero as numero, saldo, limite, rendimento, idAgencia, a.nome, a.numero as numero_agencia from conta inner join agencia as a on conta.idAgencia = a.id"

      if agencia
        query << " where idAgencia = #{agencia.id}"
      end

      resp = db.query(query)
    rescue
      return contas
    end

    resp.each do |r|
      c = Conta.new(r['numero'])

      c.id = r['id_conta']
      c.saldo = r['saldo']
      c.limite = r['limite']
      c.rendimento = r['rendimento']
      c.nome_agencia = r['nome']
      c.numero_agencia = r['numero_agencia']
      c.id_agencia = r['idAgencia']

      contas << c
    end

    contas
  end

  def salvar(agencia)
    begin
      @db.query("insert into conta(numero, saldo, limite, rendimento, idAgencia) values('#{@numero}', #{@saldo}, #{@limite}, #{@rendimento}, #{agencia.id})")
      true
    rescue
      false
    end
  end

  def atualizar(atualizar_saldo = false)
    begin
      complemento = ''

      if atualizar_saldo
        complemento = "saldo = #{@saldo},"
      end

      @db.query("update conta set numero = '#{@numero}', #{complemento} limite = #{@limite}, rendimento = #{@rendimento}, idAgencia = #{@id_agencia} where id = #{@id}")
      true
    rescue
      false
    end
  end

  def deletar
    begin
      @db.query "delete from conta where id = #{@id}"
      true
    rescue
      false
    end
  end

  def depositar(quantia)

    total = @saldo + quantia.to_i

    if total <= @limite
      @saldo = total
      self.atualizar(true)
      return true
    end
    false
  end

  def sacar(quantia)

    novo_saldo = @saldo - quantia.to_i

    if novo_saldo >= 0
      @saldo = novo_saldo
      self.atualizar(true)
      return true
    end
    false
  end

  def transferir(conta, quantia)

    quantia = quantia.to_i

    if quantia > self.saldo
      return false
    end

    if (conta.limite - conta.saldo) < quantia
      return false
    end

    ct_from = self.sacar quantia
    ct_to = conta.depositar quantia

    ct_from and ct_to
  end
end