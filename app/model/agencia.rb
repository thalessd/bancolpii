require_relative 'banco'
require_relative 'conta'

class Agencia
  attr_writer :id, :nome
  attr_reader :id, :nome, :numero

  def initialize(numero)
    @numero = numero
    @db = Banco.new
  end

  def self.listar
    agencias = []
    db = Banco.new

    resp = db.query('select * from agencia')

    resp.each do |r|
      a = Agencia.new(r['numero'])

      a.nome = r['nome']
      a.id = r['id']

      agencias << a
    end

    agencias
  end

  def salvar
    begin
      @db.query("insert into agencia(numero, nome) values('#{@numero}', '#{@nome}')")
      true
    rescue
      false
    end
  end

  def listar_contas
    Conta.listar self
  end

  def atualizar
    begin
      @db.query("update agencia set numero = '#{@numero}', nome = '#{@nome}' where id = #{@id}")
      true
    rescue
      false
    end
  end

  def deletar
    begin
      @db.query "delete from conta where idAgencia = #{@id}"
      @db.query "delete from agencia where id = #{@id}"
      true
    rescue
      false
    end
  end
end