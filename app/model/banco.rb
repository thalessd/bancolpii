require 'mysql2'

class Banco

  def self.config(nome, usuario, senha, host, porta = 3306)
    @@nome = nome
    @@usuario = usuario
    @@senha = senha
    @@host = host
    @@porta = porta
  end

  def query(q)
    client = Mysql2::Client.new(
      host: @@host,
      username: @@usuario,
      database: @@nome,
      port: @@porta,
      password: @@senha
    )

    client.query(q)
  end

end