require_relative 'view/principal'
require_relative 'model/banco'

class Main
  def initialize
    Banco.config('banco', 'root', '', '127.0.0.1')

    Principal.new
  end
end

Main.new